from pymysql import cursors


unusable_token = ''
token = '595215106:AAFfGW9kv8wVDSomA5oFu1sOCd5cC3GjqE4'

bee = 'CAADAQADlwoAAllsewoj1yBbXfFTNgI'
Stickers = {
    'strawberry' : 'CAADAgAD7wMAAkKvaQABY7gjSmhx8OMC',
    'pepper' : 'CAADAgADDQQAAkKvaQAB20Jq6ra4X8IC',
    'coconut' : 'CAADAgAD6gsAAkKvaQABHyyRoebgBmwC',
    'squirrel' : "CAADAgADIwUAAulVBRjJg60TGpQp2AI",
    'foxy_five' : "CAADAgADKgcAAkKvaQABHW9iM5VgSnkC",
    'foxy_catch' : 'CAADAgADSAcAAkKvaQABXr-0lVDyhpUC',
    'mandarin' : 'CAADAgADBwQAAkKvaQAB9Vt3ZFA4YEoC',
    'heart' : 'CAADAgAD6AsAAkKvaQABGm-IifFkubkC',
    'cherrys' : 'CAADAgAD3wMAAkKvaQABQDrUK3rT3XYC',
    'garnet' : 'CAADAgADBQQAAkKvaQABYr6eR58BugsC',
    'pumpkin' : 'CAADAgAD9gsAAkKvaQABh-ftpONKEQ8C',
}

MySQL = {
'user': 'stihovskiy',
'password':'ChangeMe1!',
'db': 'tgmbot_db',
'charset': 'utf8',
'cursorclass': cursors.SSCursor
}

days_of_week = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"]


class Inline_messages:
    msg_course_is_coming = '''
Сегодня в {0} проходит {1}.
'''
    msg_question = "Ты будешь?"
    msg_confirm = "Принято!😁"
    msg_decline = "Принято!😭"

class Messages_to_admin:
    msg_new_tester = '''
    @{user} ({id}) записался в тестировщики!
'''
    msg_stats = '''
Информация о {0} на 03.05.2018!
Точно прийдут: {1} человек,
Точно НЕ прийдут: {2} человек,
Не ответило: {3} человек.
'''
    msg_help = '''
С помощью этого бота вы можете:
/help - выводить это сообщение
/check - просмотр истории посещения
/append_course - добавлять курс в расписание
/remove_course - удалять курс из расписания
/edit_course - изменять параметры курса
/delete_user - удалить пользователя
'''
    class msg_append:
        step_setName = '''
Пожалуйста, отправь следующим сообщением название курса.
        '''
        step_setDayOfWeek = '''
Укажи, в какой день будет проходить курс.
        '''
        step_setTime = '''
Укажи время, в которое будет начинаться курс, разделяя часы и минуты двоеточием.
Как пример - "18:30"
        '''
        step_setMentor = '''
Есть ли ментор для этого курса в моей Базе Данных?
Выбери всех менторов для курса, после нажми "Конец.".
Если ментора для курса нет в Базе Данных, можно просто нажать "Конец.".
Ментора можно будет добавить позже через команду "/edit_course"
        '''
        step_info = '''
Готово! Информация по курсу:
Название: {course_name}
Курс ведут: {mentor_link}
Расписание: {timetable_info}
        '''
        
        step_isSelfDestruct = '''
Этот курс пройдёт только один раз?
Стоит выбирать "Да" для непостоянных мероприятий.
        '''
        step_setTimetable = '''
Пожалуйста, введи дату, на когда мне следует записать курс.
Следует вводить в формате "18 30 31 май", где:
18 - часы,
30 - минуты,
31 - день месяца,
май - месяц.
Пункты следует разделять пробелом. В противном случае может быть ошибка.
        '''

class Messages:

    msg_hello = "Привет, {user_id}!"
    msg_DB = "Тебя еще нет в Базе Данных!\nЗаписываю в Базу..."
    msg_DB_whoops = "Что-то пошло не так!😱\nПопробуй еще раз.\nЕсли снова что-то пойдет не так, скажи об этом @STih07"

    msg_help = '''
Этот бот призван помочь резидентам AtomSpace помнить о лекциях, которые они посещают, а лекторам - проверять \
и отслеживать посещаемость их лекций. С помощью этого бота ты можешь:
/help - выводить это сообщение
/append - добавлять курсы и лекции, по которым тебе будут приходить уведомления
/remove - удалять эти самые курсы и лекции
/user - узнать, какие курсы посещает определенный резидент
/info - выдавать информацию по выбранным тобой курсам
'''

    msg_append = "Выбери курсы, о которых ты хочешь получать уведомления:"
    msg_remove = "Выбери курсы, от уведомлений которых ты хочешь отказаться:"
    msg_info = "О каком курсе ты хочешь получить информацию?"

    msg_whoops = "Упс! Что-то пошло не так."
    msg_NO_COURSE = "Упс! Такого курса не существует!"
    msg_NO_STATE = "Не могу понять, что ты пытаешься сделать.\nВведи /help и узнай, как мной пользоваться."
    msg_ACCESS_DENIED = "У тебя недостаточно прав для выполнения этого действия!"
    msg_NO_USER = "Упс! Такого пользователя не существует!"
    msg_success = "Всё прошло успешно!"
    msg_select_user_info = "Нажми на пользователя, о котором ты хочешь получить информацию:"
    msg_user_info = """
<a href='tg://user?id={0}'>{1} {2}</a>
Посещает: {3}
"""
    msg_about_course = "Название курса: {0}\nКурс ведут: {1}\n\nРасписание: {2}⏰\n\nКурс посещают:{3}"
    msg_about_users_in_course = "На последнем занятии было {0} из {1}."
    msg_okey = "😉"


class REGEXP:
    end = "Конец."
    send_to = "Отправь"
