import telebot
import config
import time
from utils import mysql_get, mysql_set

bot = telebot.TeleBot(config.token)

today_unix = time.time()+1222
#today_unix = 1529418600
today = time.localtime(today_unix)


timestamp = time.strftime("%Y-%m-%d", today)
if today.tm_min < 10:
    user_time = f"{time.localtime(today_unix+21600).tm_hour}:0{today.tm_min}"
    mentor_time = f"{time.localtime(today_unix+16000).tm_hour}:0{today.tm_min}"
else:
    user_time = f"{time.localtime(today_unix+21600).tm_hour}:{today.tm_min}"
    mentor_time = f"{time.localtime(today_unix+16000).tm_hour}:{today.tm_min}"

#bot.send_message(242307634, user_time + "\n" + mentor_time + "\n" + str(today.tm_wday+1))
bot.send_message(242307634, f"{timestamp} {user_time}")
existed_courses = mysql_get(f"SELECT * FROM week_timetable WHERE weekday = {today.tm_wday+1} AND time = '{user_time}';")
for course in existed_courses:
    bot.send_message(242307634, "Споймал!")
    keyboard = telebot.types.InlineKeyboardMarkup()
    confirm_button = telebot.types.InlineKeyboardButton(text="Я точно буду!", callback_data=f"inline_query|1|{course[0]}|{timestamp} {user_time}:00")
    decline_button = telebot.types.InlineKeyboardButton(text="Меня не будет!", callback_data=f"inline_query|0|{course[0]}|{timestamp} {user_time}:00")
    keyboard.add(confirm_button)
    keyboard.add(decline_button)
    text_to_send = f'''
Сегодня, {time.strftime("%d.%m", today)}, в {user_time} проходит {course[1]}.\n
    ''' + config.Inline_messages.msg_question
    users = mysql_get(f'''SELECT users.chat_id FROM users INNER JOIN users_in_courses
    ON users_in_courses.user_id = users.chat_id AND users.permission = 0 
    INNER JOIN courses ON courses.id = users_in_courses.course_id AND courses.course_name = '{course[1]}';''')
    for user in users:
        bot.send_message(user[0], text_to_send, reply_markup = keyboard)

mentor_courses = mysql_get(f"SELECT * FROM courses WHERE weekday = {today.tm_wday+1} AND time = '{mentor_time}' AND active=1;")
for course in mentor_courses:
    my_query = '''SELECT chat_id, first_name, last_name FROM users 
INNER JOIN users_attendance ON users.chat_id = users_attendance.user_id 
AND users_attendance.course_id = {course_id} 
AND users_attendance.presence = {presence}
AND users_attendance.timetable = '{timetable}';
'''
    true_users = ""
    true_u = mysql_get(my_query.format(course_id = course[0], presence = 1, timetable = f"{timestamp} {mentor_time}"))
    for i in range(len(true_u)):
        true_users += "\n{0}. <a href='tg://user?id={1}'>{2} {3}</a>".format(i+1, true_u[i][0], true_u[i][1], true_u[i][2])
    false_users = ""
    false_u = mysql_get(my_query.format(course_id = course[0], presence = 0, timetable = f"{timestamp} {mentor_time}"))
    for i in range(len(false_u)):
        false_users += "\n{0}. <a href='tg://user?id={1}'>{2} {3}</a>".format(i+1, false_u[i][0], false_u[i][1], false_u[i][2])
    text_to_mentor = f'''
Информация на {timestamp}.
Те, кто <b>будут</b> на {course[1]}: {true_users}

Те, кого <b>не будет</b> на {course[1]}: {false_users}
'''
    bot.send_message(242307634, text_to_mentor, parse_mode = "HTML")
#     user_list = mysql_get(f'''SELECT chat_id FROM users 
# INNER JOIN users_in_courses ON users_in_courses.user_id = users.chat_id
# AND course_id = {course[0]};''')
#     for user in user_list:
#         bot.send_message(user[0], text_to_send, reply_markup = )
    


current_time = f"{timestamp} {user_time}"
onetime_events = mysql_get(f"SELECT * FROM events WHERE time_to_send = {current_time}:00;")
for event in onetime_events:
    keyboard = telebot.types.InlineKeyboardMarkup()
    confirm_button = telebot.types.InlineKeyboardButton(text="Да!", callback_data=f"inline_event_query|1|{event[0]}|{current_time}:00")
    decline_button = telebot.types.InlineKeyboardButton(text="Нет!", callback_data=f"inline_event_query|0|{event[0]}|{current_time}:00")
    keyboard.add(confirm_button)
    keyboard.add(decline_button)
#   user_list = mysql_get(f'''SELECT chat_id FROM users 
#   INNER JOIN users_in_courses ON users_in_courses.user_id = users.chat_id AND users.permission = 0;''')
    # for user in user_list:
    bot.send_message(242307634, event[1], reply_markup = keyboard)
