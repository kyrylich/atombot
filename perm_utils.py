import telebot
import pymysql
import utils
from utils import mysql_set, mysql_get
from config import *

bot = telebot.TeleBot(token)

class Course_to_append:
    def __init__(self, name):
        self.name = name
        self.day = None
        self.Time = None
        self.timetable = None
        self.mentors = {}
        self.destruct = False


def check_permissions(message):
    conn = pymysql.connect(**MySQL)
    cursor = conn.cursor()
    cursor.execute("SELECT permission FROM users WHERE chat_id = {0}".format(message.from_user.id))
    perm = cursor.fetchone()
    conn.close()
    return perm[0]


def send_all_courses(message):
    course_list = mysql_get('SELECT course_name FROM courses ORDER BY id')
    markup = utils.get_another_markup(course_list)
    bot.send_message(message.from_user.id, Messages.msg_info, reply_markup = markup)
        

def get_datetime(message):
    datetime_list = mysql_get(f'''SELECT users_attendance.timetable FROM users_attendance 
    INNER JOIN courses ON courses.course_name = '{message.text}'
    AND courses.id = users_attendance.course_id GROUP BY users_attendance.timetable;''')
    keyboard = telebot.types.InlineKeyboardMarkup()
    for elem in datetime_list:
        keyboard.add(telebot.types.InlineKeyboardButton(text=str(elem[0]), callback_data=f"admin_check_query|{message.text}|{str(elem[0])}"))
    bot.send_message(message.from_user.id, "Список дат:", reply_markup=keyboard)

def create_course(message):
    try:
        course_name = message.text
        future_course = Course_to_append(course_name)
        markup = telebot.types.ReplyKeyboardMarkup(row_width=3, selective=True, resize_keyboard=True, one_time_keyboard=True)
        utils.get_markup_row(["Да", "Нет"], markup)
        bot.send_message(message.chat.id, Messages_to_admin.msg_append.step_isSelfDestruct, reply_markup=markup)
        bot.register_next_step_handler(message, create_course_step_isSelfDestruct)
    except Exception:
        bot.send_message(message.chat.id, Messages.msg_whoops)

def create_course_step_isSelfDestruct(message):
    if message.text == "Нет":
        self.destruct = False
        markup = telebot.types.ReplyKeyboardMarkup(row_width=3, selective=True, resize_keyboard=True, one_time_keyboard=True)
        utils.get_markup_row(days_of_week, markup)
        bot.send_message(message.chat.id, Messages_to_admin.msg_append.step_setDayOfWeek, reply_markup=markup)
        bot.register_next_step_handler(message, create_course_step_setDayOfWeek)
    elif message.text == "Да":
        self.destruct = True
        

def create_course_step_setDayOfWeek(message):
    try:
        future_course.day = str(days_of_week.index(message.text))
        bot.send_message(message.chat.id, Messages_to_admin.msg_append.step_setTime)
        bot.register_next_step_handler(message, create_course_step_3)
    except Exception:
        bot.send_message(message.chat.id, Messages.msg_whoops)

# def create_course_step_4(message):
#     try:
#         future_course.Time = message.text.split(":")
#         future_course.timetable = ""