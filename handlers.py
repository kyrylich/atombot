import config
import telebot
import utils
import perm_utils
from random import choice

bot = telebot.TeleBot(config.token)


def request_command(message):
    if message.from_user.username:
        bot.send_message(message.from_user.id, config.Messages.msg_hello.format(
            user_id="@"+str(message.from_user.username)))
    else:
        bot.send_message(message.from_user.id, config.Messages.msg_hello.format(
            user_id=message.from_user.first_name))
    utils.check_user(message)


def help_command(message):
    # if perm_utils.check_permissions(message) != 2:
    bot.send_message(message.chat.id, config.Messages.msg_help)
    # else:
    #     bot.send_message(message.chat.id, config.Messages_to_admin.msg_help)


def remove_command(message):
    if utils.get_state(message):
        utils.set_state(message, 'remove_mode')
        res = utils.get_active_courses(message)
        markup = telebot.types.ReplyKeyboardMarkup(
            row_width=3, selective=True, resize_keyboard=True)
        utils.get_markup_row(res, markup)
        bot.send_message(
            message.chat.id, config.Messages.msg_remove, reply_markup=markup)
    else:
        bot.send_message(message.chat.id, "Эта команда только для резидентов😔")


def append_command(message):
    if utils.get_state(message):
        utils.set_state(message, 'append_mode')
        res = utils.get_inactive_courses(message)
        markup = telebot.types.ReplyKeyboardMarkup(
            row_width=3, selective=True, resize_keyboard=True)
        utils.get_markup_row(res, markup)
        bot.send_message(
            message.chat.id, config.Messages.msg_append, reply_markup=markup)
    else:
        bot.send_message(message.chat.id, "Эта команда только для резидентов😔")


def append_course_command(message):
    bot.send_message(
        message.chat.id, config.Messages_to_admin.msg_append.step_setName)
    bot.register_next_step_handler(message, perm_utils.create_course)


def info_command(message):
    if utils.get_state(message):
        utils.set_state(message, 'info_mode')
        res = utils.transform_courses_to_markup()
        markup = telebot.types.ReplyKeyboardMarkup(
            row_width=3, selective=True, one_time_keyboard=True, resize_keyboard=True)
        utils.get_markup_row(res, markup)
        bot.send_message(
            message.chat.id, config.Messages.msg_info, reply_markup=markup)
    else:
        bot.send_message(message.chat.id, "Эта команда только для резидентов😔")


def user_command(message):
    users = utils.get_users_list(0)
    markup = telebot.types.ReplyKeyboardMarkup(selective=True)
    utils.get_markup_row(users, markup)
    bot.send_message(
        message.chat.id, config.Messages.msg_select_user_info, reply_markup=markup)


def set_as_tester(message):
    bot.send_message(242307634, config.Messages_to_admin.msg_new_tester.format(user=message.from_user.username,
                                                                               id=message.from_user.id))


def end_button(message):
    utils.set_state(message, 'default_mode')
    bot.send_message(message.chat.id, config.Messages.msg_okey,
                     reply_markup=telebot.types.ReplyKeyboardRemove())


def any_text(message):
    key = utils.key_existence(message)
    if not key:
        bot.send_message(message.from_user.id, config.Messages.msg_NO_COURSE)
    else:
        user_state = utils.get_state(message)
        if user_state[0] == utils.user_state['default_mode']:
            bot.send_message(message.from_user.id,
                             config.Messages.msg_NO_STATE)
        elif user_state[0] == utils.user_state['remove_mode']:
            utils.remove_course(message)
            res = utils.get_active_courses(message)
            markup = telebot.types.ReplyKeyboardMarkup(
                row_width=3, selective=True, resize_keyboard=True)
            utils.get_markup_row(res, markup)
            bot.send_message(
                message.chat.id, config.Messages.msg_remove, reply_markup=markup)
        elif user_state[0] == utils.user_state['append_mode']:
            utils.append_course(message)
            res = utils.get_inactive_courses(message)
            markup = telebot.types.ReplyKeyboardMarkup(
                row_width=3, selective=True, resize_keyboard=True)
            utils.get_markup_row(res, markup)
            bot.send_message(
                message.chat.id, config.Messages.msg_append, reply_markup=markup)
        elif user_state[0] == utils.user_state['info_mode']:
            results = utils.get_info_to_string(message.text, 0)
            course_time = utils.get_timetable(message.text)
            course_mentor = utils.get_info_to_string(message.text, 1)
            msg_info = config.Messages.msg_about_course.format(
                message.text, course_mentor, course_time, results)
            bot.send_message(message.chat.id, msg_info, parse_mode="HTML")
        else:
            bot.send_message(message.from_user.id, '''Не могу найти тебя в списке.
Ты точно резидент?''')


def inline_handler(call):
    if "inline_query" in call.data:
        user_choice = call.data.split("|")
        utils.mysql_set('''
INSERT INTO users_attendance (user_id, course_id, timetable, presence)
VALUES ({id}, {course_id}, "{time}", {presence});
'''.format(id=call.message.chat.id, course_id=user_choice[2], time=user_choice[3], presence=user_choice[1]))
        text_to_send = call.message.text
        if user_choice[1] == "1":
            text_to_send = text_to_send.replace(
                config.Inline_messages.msg_question, config.Inline_messages.msg_confirm)
        elif user_choice[1] == "0":
            text_to_send = text_to_send.replace(
                config.Inline_messages.msg_question, config.Inline_messages.msg_decline)
        bot.edit_message_text(chat_id=call.message.chat.id,
                              message_id=call.message.message_id, text=text_to_send)
    elif "inline_event_query" in call.data:
        user_choice = call.data.split("|")
        utils.mysql_set('''
INSERT INTO events_choices (user_id, event_id, timetable, presence)
VALUES ({id}, {event_id}, "{time}", {presence});
'''.format(id=call.message.chat.id, event_id=user_choice[2], time=user_choice[3], presence=user_choice[1]))
        bot.edit_message_text(chat_id=call.message.chat.id,
                              message_id=call.message.message_id, text="Принято!")
    elif "admin_check_query" in call.data:
        admin_choice = call.data.split("|")
        query = '''SELECT chat_id, first_name, last_name FROM users 
        INNER JOIN users_attendance ON users.chat_id = users_attendance.user_id AND users_attendance.presence = {presence} 
        AND users_attendance.timetable = '{timetable}'
        INNER JOIN courses ON courses.course_name = '{course_name}' AND courses.id = users_attendance.course_id;
        '''
        true_users = ""
        true_u = utils.mysql_get(query.format(
            course_name=admin_choice[1], presence=1, timetable=admin_choice[2]))
        for i in range(len(true_u)):
            true_users += "\n{0}. <a href='tg://user?id={1}'>{2} {3}</a>".format(
                i+1, true_u[i][0], true_u[i][1], true_u[i][2])
        false_users = ""
        false_u = utils.mysql_get(query.format(
            course_name=admin_choice[1], presence=0, timetable=admin_choice[2]))
        for i in range(len(false_u)):
            false_users += "\n{0}. <a href='tg://user?id={1}'>{2} {3}</a>".format(
                i+1, false_u[i][0], false_u[i][1], false_u[i][2])
        text_to_admin = f'''
        Информация о {admin_choice[2]}.
Те, кто были на {admin_choice[1]}: {true_users}

Те, кого не было на {admin_choice[1]}: {false_users}
'''
        bot.send_message(call.message.chat.id,
                         text_to_admin, parse_mode="HTML")


def any_sticker(message):
    if message.sticker.file_id == config.bee:
        bot.send_sticker(message.chat.id, config.bee)
        bot.send_message(242307634, "@{0} вызвал пчёлку!".format(
            message.from_user.username))
    else:
        bot.send_sticker(
            message.chat.id, config.Stickers[choice(list(config.Stickers))])
