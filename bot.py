import config
import telebot
import utils
import handlers
import perm_utils
import logging

#logger = telebot.logger
#telebot.logger.setLevel(logging.DEBUG)


bot = telebot.TeleBot(config.token)

################################

@bot.message_handler(commands=["start", "help"])
def help_command(message):
    handlers.help_command(message)

@bot.message_handler(commands=["request"])
def request_command(message):
    handlers.request_command(message)

@bot.message_handler(commands=["remove"])
def remove_command(message):
    handlers.remove_command(message)

@bot.message_handler(commands=["mysql_get"])
def get_data(message):
    if message.chat.id == 242307634:
        bot.send_message(message.chat.id, "Вводи запрос")
        bot.register_next_step_handler(message, utils.send_data)

@bot.message_handler(commands=["mysql_set"])
def save_data(message):
    if message.chat.id == 242307634:
        bot.send_message(message.chat.id, "Вводи запрос")
        bot.register_next_step_handler(message, utils.save_data)

@bot.message_handler(commands=["append"])
def append_command(message):
    handlers.append_command(message)

@bot.message_handler(commands=["append_course"])
def append_course(message):
    handlers.append_course_command(message)


@bot.message_handler(commands=["info"])
def info_command(message):
    handlers.info_command(message)

@bot.message_handler(commands=["user"])
def user_command(message):
    handlers.user_command(message)
    bot.register_next_step_handler(message, utils.get_user_info)

@bot.message_handler(commands=["check"])
def check_course(message):
    if perm_utils.check_permissions(message) != 2:
        bot.send_message(message.from_user.id, config.Messages.msg_ACCESS_DENIED)
    else:
        perm_utils.send_all_courses(message)
        bot.register_next_step_handler(message, perm_utils.get_datetime)

@bot.message_handler(commands=["test"])
def set_as_tester(message):
    handlers.set_as_tester(message)

@bot.message_handler(regexp = config.REGEXP.end)
def end_button(message):
    handlers.end_button(message)


@bot.message_handler(content_types=['text'])
def any_text(message):
   handlers.any_text(message)



@bot.message_handler(content_types=['sticker'])
def send_bee(message):
    handlers.any_sticker(message)

@bot.callback_query_handler(func=lambda call: True)
def inline_handler_for_everything(call):
    handlers.inline_handler(call)

################################


bot.polling(none_stop=True, timeout=120)
