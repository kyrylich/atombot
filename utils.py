import telebot
import pymysql
from config import *


bot = telebot.TeleBot(token)
user_state = {'default_mode': 0, 'remove_mode': 1, 'append_mode': 2, 'info_mode': 3}


def mysql_get(query):
    conn = pymysql.connect(**MySQL)
    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    conn.close()
    return result


def mysql_set(query):
    conn = pymysql.connect(**MySQL)
    cursor = conn.cursor()
    cursor.execute(query)
    conn.commit()
    conn.close()


def send_data(message):
    result = mysql_get(message.text)
    text_to_send = ""
    for element in result:
        text_to_send += str(element) + "\n"
    bot.send_message(message.chat.id, text_to_send)

def save_data(message):
    mysql_set(message.text)
    bot.send_message(message.chat.id, "Всё получилось!")

def get_courses_list():
    result = mysql_get('SELECT course_name, id from courses WHERE onetime = 0 ORDER BY id')
    return result

courses_list = dict(get_courses_list())

def transform_courses_to_markup():
    res = get_courses_list()
    for n in range(len(res)):
        res[n] = res[n][0]
    return res

def check_user(message):
    conn = pymysql.connect(**MySQL)
    cursor = conn.cursor()
    cursor.execute('SELECT chat_id FROM users WHERE chat_id={0};'.format(message.from_user.id))
    result = cursor.fetchone()
    if not result:
        bot.send_message(message.from_user.id, Messages.msg_DB)
        if not message.from_user.last_name:
            message.from_user.last_name = ""
        if not message.from_user.username:
            message.from_user.username = ""
        try:
            cursor.execute('INSERT INTO users (chat_id, username, first_name, last_name, permission, user_state) \
            VALUES ({0}, "{1}", "{2}", "{3}", 0, 0);'.\
format(message.from_user.id, message.from_user.username, message.from_user.first_name, message.from_user.last_name))
#            cursor.execute('''INSERT INTO users_in_courses(user_id, course_id, permission)
#            SELECT "{0}", 1, 0
#            WHERE NOT EXISTS(SELECT 1 FROM users_in_courses
#            WHERE user_id = "{0}" AND course_id = 1);"'''.format(message.from_user.id))
            conn.commit()
            conn.close()
        except:
            bot.send_message(message.from_user.id, Messages.msg_whoops + '\n' + Messages.msg_DB_whoops)
        else:
            bot.send_message(message.from_user.id, Messages.msg_success)
            # bot.send_message(242307634,"@" + message.chat.username + " теперь в базе данных!")


def set_state(message, key):
    mysql_set('UPDATE users SET user_state = {0} WHERE chat_id = {1}'.format(user_state[key], message.from_user.id))


def get_state(message):
    conn = pymysql.connect(**MySQL)
    cursor = conn.cursor()
    cursor.execute('SELECT user_state FROM users WHERE chat_id={chid}'.format(chid=message.chat.id))
    state = cursor.fetchone()
    conn.close()
    return state


def get_info(course_name, permission):
        course_id = courses_list[course_name]
        result = mysql_get('''SELECT chat_id, first_name, last_name, chat_id FROM users INNER JOIN users_in_courses ON
users_in_courses.user_id = users.chat_id AND users_in_courses.permission = {0}
WHERE course_id = {1}'''.format(permission, course_id))
        return result


def get_info_to_string(course_name, permission):
    users_list = ""
    users = get_info(course_name, permission)
    for i in range(len(users)):
        users_list += "\n{0}. <a href='tg://user?id={1}'>{2} {3}</a>".format(i+1, users[i][0], users[i][1], users[i][2])
    return users_list


def get_users_list(perm_lvl):
    users = mysql_get("SELECT id, first_name, last_name FROM users WHERE permission = {0} ORDER BY id".format(perm_lvl))
    for i in range(len(users)):
        users[i] = str('{0}. {1} {2}'.format(users[i][0], users[i][1], users[i][2]))
    return users

def get_user_info(message):
    try:
        user = message.text.split()
        if len(user) < 3:
            user.append("")
        conn = pymysql.connect(**MySQL)
        cursor = conn.cursor()
        cursor.execute("SELECT chat_id FROM users WHERE first_name LIKE '%{0}%' AND last_name LIKE '%{1}%'".\
    format(user[1], user[2]))
        user[0] = cursor.fetchone()[0]
        cursor.execute("SELECT course_name FROM courses INNER JOIN users_in_courses \
    ON courses.id = users_in_courses.course_id \
    AND users_in_courses.user_id = {0}".format(user[0]))
        courses_string = ""
        courses = cursor.fetchall()
        conn.close()
        for c in range(len(courses)):
            courses_string += "\n{0}. {1}".format(c+1, courses[c][0])
        if not courses:
            courses = "Этот резидент ничего не посещает."
        try:
            bot.send_message(message.chat.id, Messages.msg_user_info.format(user[0], user[1], user[2], courses_string), \
parse_mode="HTML", reply_markup=telebot.types.ReplyKeyboardRemove())
        except Exception as exp:
            bot.send_message(242307634, exp)
            bot.send_message(message.chat.id, Messages.msg_DB_whoops, reply_markup=telebot.types.ReplyKeyboardRemove())
    except:
            bot.send_message(message.chat.id, Messages.msg_NO_USER, reply_markup=telebot.types.ReplyKeyboardRemove())


def get_active_courses(message):
    res = mysql_get('SELECT course_name FROM courses INNER JOIN users_in_courses \
    ON users_in_courses.user_id = {0} AND users_in_courses.course_id = courses.id AND courses.onetime = 0;'.format(message.from_user.id))
    for n in range(len(res)):
        res[n] = res[n][0]
    return res


def get_inactive_courses(message):
    res = mysql_get('SELECT course_name FROM courses \
WHERE courses.id NOT IN (SELECT course_id FROM users_in_courses WHERE users_in_courses.user_id = {0} GROUP BY course_id) \
AND courses.onetime = 0 GROUP BY courses.id'.format(message.from_user.id))
    for n in range(len(res)):
        res[n] = res[n][0]
    return res

def get_another_markup(data_array):
    markup = telebot.types.ReplyKeyboardMarkup(row_width=3, selective=True, one_time_keyboard=True, resize_keyboard=True)
    button_list = [data_array[i][0] for i in range(len(data_array))]
    while len(button_list)%3!=0:
        button_list.append("")
    for n in range(0, len(button_list), 3):
        markup.row(button_list[n], button_list[n+1], button_list[n+2])
    return markup

def get_markup_row(res, markup):
    for n in range(len(res)):
        res[n] = telebot.types.KeyboardButton(str(res[n]))
    res.append(telebot.types.KeyboardButton(REGEXP.end))
    while len(res)%3!=0:
        res.append("")
    for step in range(len(res)):
        if step%3 == 0:
            markup.row(res[step], res[step+1], res[step+2])
    return markup


def key_existence(message):
    try:
        key = courses_list[message.text]
    except KeyError:
        return 0
    else:
        return key


def remove_course(message):
    mysql_set("DELETE FROM users_in_courses WHERE users_in_courses.user_id = {0}  \
and users_in_courses.course_id = {1}".format(message.from_user.id, courses_list[message.text]))


def append_course(message):
    user_data = [message.from_user.id, courses_list[message.text], 0]
    result = mysql_get("SELECT * FROM users_in_courses WHERE user_id = {0} AND course_id = {1}".format(*user_data))
    if result:
        bot.send_message(message.from_user.id, Messages.msg_whoops)
    else:
        mysql_set("INSERT INTO users_in_courses (user_id, course_id, permission) VALUES ({0}, {1}, {2})".format(*user_data))


def get_timetable(course_name):
    conn = pymysql.connect(**MySQL)
    cursor = conn.cursor()
    cursor.execute("SELECT timetable FROM courses WHERE course_name = '{0}'".format(course_name))
    course_time = cursor.fetchone()
    conn.close()
    return course_time[0]

