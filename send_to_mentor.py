import telebot
import config
from utils import mysql_get, get_info_to_string
# import inline_utils

bot = telebot.TeleBot(config.token)
my_query = '''SELECT chat_id, first_name, last_name FROM users 
INNER JOIN users_attendance ON users.chat_id = users_attendance.user_id 
AND users_attendance.course_id = -7 
AND users_attendance.presence = {0};
'''
true_users = ""
true_u = mysql_get(my_query.format(1))
for i in range(len(true_u)):
    true_users += "\n{0}. <a href='tg://user?id={1}'>{2} {3}</a>".format(i+1, true_u[i][0], true_u[i][1], true_u[i][2])
false_users = ""
false_u = mysql_get(my_query.format(0))
for i in range(len(false_u)):
    false_users += "\n{0}. <a href='tg://user?id={1}'>{2} {3}</a>".format(i+1, false_u[i][0], false_u[i][1], false_u[i][2])

text_to_send = '''
Те, кто будут на лекции "Всё о наркотиках": {0}

Те, кого не точно не будет на лекции "Всё о наркотиках": {1}
'''.format(true_users, false_users)
# bot.send_message(340915192, text_to_send, parse_mode = "HTML")
# bot.send_message(170741198, text_to_send, parse_mode = "HTML")
# bot.send_message(274351055, text_to_send, parse_mode = "HTML")
bot.send_message(242307634, text_to_send, parse_mode = "HTML")