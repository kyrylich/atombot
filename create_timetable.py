from utils import mysql_set, mysql_get

mysql_set("DELETE FROM week_timetable;")
courses_to_update = mysql_get("SELECT id, weekday, time FROM courses WHERE active = 1;")
for course in courses_to_update:
    mysql_set(f"INSERT INTO week_timetable VALUES ({course[0]}, {course[1]-1}, '{course[2]}');")